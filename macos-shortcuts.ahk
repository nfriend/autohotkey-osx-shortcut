; Only run one instance of this script
#SingleInstance forcen

; Close the current application with ctrl+Q
^q::
send !{f4}
return

; Remap the spotlight shortcut (ctrl+space) to the Windows key
; From https://superuser.com/a/1089151/144803
^space::
send ^{esc}
return

; Map ctrl+tab to alt+tab
ctrl & tab::AltTab
; shift & ctrl & tab::ShiftAltTab